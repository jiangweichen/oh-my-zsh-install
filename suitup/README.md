## Environment

- Ubuntu
- bash/zsh

## Install

    ./install.sh

## Command

All commands hava a prefixed is "suitup".

- Open current folder

        oc

        suitup-open-current

- Open a file

        o a-file-path

        suitup-open a-file-path

- Git show diff

        suitup-git-diff

- Git commit

        suitup-git-commit [message]

- Git commit push

        suitup-git-commit-push [message]
